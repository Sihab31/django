from .models import Peralatan # baru gaes
from django.shortcuts import render
from django.views.generic import DetailView, CreateView #baru gaes
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import View#baru
from django.urls import reverse_lazy
from .utils import Render

# Create your views here.
var = {
    'judul' : 'Toko Pertukangan Aneka Kerja',
     'info' : '''Kami menyediakan segala peralatan dan perkakas pertukangan Anda,
         baik untuk hobi maupun untuk profesi anda''',
     'oleh' : 'Sihab dan Nada'
}
def index(self):
    #baru gaes .....
    var['peralatan'] = Peralatan.objects.values('id','nama_alat','kategori').\
         order_by('nama_alat')
    return render(self, 'perkakas/index.html',context=var)

class AlatDetailView(DetailView): #baru gaes
    model = Peralatan
    template_name = 'perkakas/alat_detail_view.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context
        
class AlatCreateView(CreateView): #baru
    model = Peralatan
    fields = '__all__'
    template_name = 'perkakas/alat_add.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class AlatEditView(UpdateView):
    model = Peralatan
    fields = ['nama_alat','kategori','keterangan',
              'berat','harga','jumlah']
    template_name = 'perkakas/alat_edit.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class AlateDeleteView(DeleteView):
    model = Peralatan
    template_name = 'perkakas/alat_delete.html'
    success_url = reverse_lazy('home_page')

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context


class AlatToPdf(View):
    def get(self,request):
        var = {
            'peralatan' : Peralatan.objects.values(
                'nama_alat','kategori','harga','keterangan'),
            'request':request
        }
        return Render.to_pdf(self,'perkakas/alat_to_pdf.html',var)


